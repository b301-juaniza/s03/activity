package com.zuitt;

import java.util.Scanner;

public class LoopsException {
    public static void main(String[]args){
        Scanner scan = new Scanner(System.in);

        int answer = 1;
        int counter = 1;
        int num = 0;
        try {
            System.out.println("Input an integer whose factorial will be computed:");
            num = scan.nextInt();
        } catch (Exception e){
            System.out.println("Input a valid Number!");
            e.printStackTrace();
        }
        if (num == 0){
            System.out.println("Number must be Greater than 0!");
            System.exit(0);
        } else if (num < 0){
            System.out.println("Cannot compute for factorials of negative numbers!");
            System.exit(0);
        }

        /* while (num > counter){
            answer *= num;
            num--;
        } */

        for (int i = num; i > 1; i--) {
            answer *= i;
        }

        System.out.println("The factorial of " + num + " is " + answer);
    }
}
